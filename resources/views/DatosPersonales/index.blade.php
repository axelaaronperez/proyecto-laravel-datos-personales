@extends('layouts.app')

@section('content')
<div class="text-center">
<h1>Datos</h1>
</div>
<div class="container">
<table class="table table-bordered">
<thead class="thead-dark">
<tr>
<th>Id</th>
<th>Nombre</th>
<th>Apellido Paterno</th>
<th>Apellido Materno</th>
<th>Fecha de Nacimiento</th>
</tr>

</thead>
<tbody>

@foreach($datop as $dat)
<tr>
<td>{{$dat->id}}</td>
<td>{{$dat->nombre}}</td>
<td>{{$dat->apellidop}}</td>
<td>{{$dat->apellidom}}</td>
<td>{{$dat->fechanacimiento}}</td>


</tr>
@endforeach

</tbody>
</table>
<a href="{{url('/DatosPersonales/create')}}" class="btn btn-primary">Agregar</a>
</div>

@endsection
