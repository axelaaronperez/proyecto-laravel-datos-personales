@extends('layouts.app')

@section('content')
<form action="{{route('DatosPersonales.store')}}" method="post">
  @csrf
 <div class="container">
    <div class="form-group">
    <input type="text" class="form-control" name="nombre" placeholder="Nombre">

    </div>
    <div class="form-group">
    <input type="text" class="form-control" name="apellidop" placeholder="Apellido Paterno">

    </div>
    <div class="form-group">
    <input type="text" class="form-control" name="apellidom" placeholder="Apellido Materno">

    </div>
    <div class="form-group">
    <input type="text" class="form-control" name="fechanacimiento" placeholder="Fecha Nacimiento">

    </div>
    <button type= "submit" class="btn btn-primary">Guardar</button>

</div>
 </form>

@endsection